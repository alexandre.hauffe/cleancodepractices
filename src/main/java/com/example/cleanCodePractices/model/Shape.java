package com.example.cleanCodePractices.model;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
public abstract class Shape{

    @Getter @Setter private int id;
    @Getter @Setter private String name;

    public Shape(String name) {
        this.name = name;
    }

    public abstract void setMetrics(Map<String, Double> metrics);
    public abstract Double getArea();
}
