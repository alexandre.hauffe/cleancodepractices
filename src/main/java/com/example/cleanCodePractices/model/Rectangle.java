package com.example.cleanCodePractices.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

public class Rectangle extends Shape{

    @Getter @Setter private Double width;
    @Getter @Setter private Double height;

    public Rectangle(String name) {
        super(name);
    }

    @Override
    public void setMetrics(Map<String, Double> metrics) {
        this.width = Double.parseDouble(metrics.get("width").toString());
        this.height = Double.parseDouble(metrics.get("height").toString());
    }

    @Override
    public Double getArea() {
        return (this.width*this.height)/2;
    }
}
