package com.example.cleanCodePractices.service;

import com.example.cleanCodePractices.dao.ShapeRepository;
import com.example.cleanCodePractices.model.Circle;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CircleService implements ShapeService<Circle>{

    @Override
    public Circle create(String name, Integer sides, Map metrics) {
        Circle circle = new Circle(name);
        circle.setMetrics(metrics);
        return (Circle) ShapeRepository.getInstance().save(circle);
    }

    @Override
    public Circle findById(int id) {
        return (Circle) ShapeRepository.getInstance().findById(id);
    }
}
