package com.example.cleanCodePractices.controller;

import com.example.cleanCodePractices.model.Shape;
import com.example.cleanCodePractices.service.ShapeFactoryService;
import com.example.cleanCodePractices.service.ShapeService;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "/")
public class ShapeController {

    @GetMapping(path = {"{sides}/{id}"})
    public Shape findById(@PathVariable int sides, @PathVariable int id) throws Exception {
        ShapeService service = ShapeFactoryService.getInstance(sides);
        return (Shape) service.findById(id);
    }

    @PostMapping(path = {"create/{name}/{sides}"})
    public Shape create(@PathVariable String name,
                        @PathVariable int sides,
                        @RequestBody Map<String, Double> metrics) throws Exception {
        ShapeService service = ShapeFactoryService.getInstance(sides);
        return (Shape) service.create(name, sides, metrics);
    }
}
