package com.example.cleanCodePractices.service;

import com.example.cleanCodePractices.dao.ShapeRepository;
import com.example.cleanCodePractices.model.Rectangle;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class RectangleService implements ShapeService<Rectangle>{

    @Override
    public Rectangle create(String name, Integer sides, Map metrics) {
        Rectangle rectangle = new Rectangle(name);
        rectangle.setMetrics(metrics);
        return (Rectangle) ShapeRepository.getInstance().save(rectangle);
    }

    @Override
    public Rectangle findById(int id) {
        return (Rectangle) ShapeRepository.getInstance().findById(id);
    }
}
