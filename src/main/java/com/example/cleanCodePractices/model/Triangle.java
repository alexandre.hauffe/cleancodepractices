package com.example.cleanCodePractices.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

public class Triangle extends Shape{

    @Getter @Setter private Double base;
    @Getter @Setter private Double height;

    public Triangle(String name) {
        super(name);
    }

    @Override
    public void setMetrics(Map<String, Double> metrics){
        this.base = Double.parseDouble(metrics.get("base").toString());
        this.height = Double.parseDouble(metrics.get("height").toString());
    }

    @Override
    public Double getArea() {
        return (this.base*this.height)/2;
    }
}
