package com.example.cleanCodePractices.dao;

import com.example.cleanCodePractices.model.Circle;
import com.example.cleanCodePractices.model.Shape;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public final class ShapeRepository {

    private static ShapeRepository instance;
    private static final List<Shape> SHAPE_DATABASE = new ArrayList<>();
    private static final AtomicInteger ID_GENERATOR = new AtomicInteger(1000);

    public Shape save(Shape shape){
        shape.setId(ID_GENERATOR.getAndIncrement());
        SHAPE_DATABASE.add(shape);
        return shape;
    }

    public Shape findById(int id) {
        return SHAPE_DATABASE.stream()
                .filter(circle -> (id == circle.getId()))
                .findAny()
                .orElse(null);
    }

    public static ShapeRepository getInstance() {
        return Optional.ofNullable(instance).orElse(instance = new ShapeRepository());
    }
}
