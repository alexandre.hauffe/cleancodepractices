package com.example.cleanCodePractices.service;

import java.util.Map;

public interface ShapeService<T> {
    T create(String name, Integer sides, Map metrics);
    T findById(int id);
}
