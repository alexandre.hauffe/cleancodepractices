package com.example.cleanCodePractices.service;
import com.example.cleanCodePractices.dao.ShapeRepository;
import com.example.cleanCodePractices.model.Triangle;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public class TriangleService implements ShapeService<Triangle>{

    @Override
    public Triangle create(String name, Integer sides, Map metrics) {
        Triangle triangle = new Triangle(name);
        triangle.setMetrics(metrics);
        return (Triangle) ShapeRepository.getInstance().save(triangle);
    }

    @Override
    public Triangle findById(int id) {
        return (Triangle) ShapeRepository.getInstance().findById(id);
    }
}
