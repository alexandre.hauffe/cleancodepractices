package com.example.cleanCodePractices.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class ShapeFactoryService {

    @Autowired
    private CircleService circleService;

    @Autowired
    private RectangleService rectangleService;

    @Autowired
    private TriangleService triangleService;

    private static final Map<Integer, ShapeService> handler = new HashMap<>();

    @PostConstruct
    private Map<Integer, ShapeService> getObject(){
      handler.put(0, circleService);
      handler.put(3, triangleService);
      handler.put(4, rectangleService);
      return handler;
    }

    public static ShapeService getInstance(Integer sides) throws Exception{
        return Optional.ofNullable(handler.get(sides)).orElseThrow(() -> new IllegalArgumentException("Invalid service"));
    }



}
