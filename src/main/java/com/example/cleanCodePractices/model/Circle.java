package com.example.cleanCodePractices.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

public class Circle extends Shape{

    @Getter @Setter private Double radius;

    public Circle(String name) {
        super(name);
    }

    @Override
    public void setMetrics(Map<String, Double> metrics){
        this.radius = Double.parseDouble(metrics.get("radius").toString());
    }

    @Override
    public Double getArea() {
        return (Math.PI * (radius * radius));
    }
}
